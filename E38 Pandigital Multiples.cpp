// E38 Pandigital Multiples.cpp : Defines the entry point for the console application.
// Lots of preliminary work has been done on this one

#include "stdafx.h"

int main()
{
	using std::vector; using std::endl; using std::cout; using std::string;
	using std::to_string; using std::stoi;
	unsigned int max_9pan = 0;
	unsigned int gives_max_9pan = 0;
	for (unsigned int i = 9182; i <= 9876; ++i) {
		string s;
		unsigned int mult = 1;
		unsigned int cur_9pan = 1;
		while (s.size() < 9) {
			s += to_string(i * mult++);
		}
		if (s.size() != 9)
			continue;
		cur_9pan = stoi(s);
		vector<bool> dig_present(9, false);
		bool zero_found = false;
		bool duplicate_found = false;
		for (int pos = 0; pos < 9; ++pos) {
			string letter = s.substr(pos, 1);
			if (letter == "0") {
				zero_found = true;
				break;
			}
			int dig = stoi(letter);
			if (dig_present[dig - 1]) {
				duplicate_found = true;
				break;
			}
			else
				dig_present[dig - 1] = true;
		}
		
		if (!duplicate_found && !zero_found) {
			if (cur_9pan > max_9pan) {
				max_9pan = cur_9pan;
				gives_max_9pan = i;
			}
		}
	}
	cout << gives_max_9pan << ": " << max_9pan;
    return 0;
}

